import React, { useState, Fragment } from 'react';
import { Link } from 'react-router-dom';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem
} from 'reactstrap';

const AppNavbar = (props) => {
  console.log(props.token)
  console.log(props.name)
  console.log(props.getCurrentMember.name)
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  let authLinks = "";
  let authLinks2 = "";
  if(!props.token) {
    authLinks = (
        <Fragment>
          <Link to="/register" className="nav-link btn btn-primary">Register</Link>
          <Link to="/login" className="nav-link btn btn-primary ml-2">Login</Link>
        </Fragment>
      )
  } else {
    authLinks = (
        <Fragment>
          <Link to="/profile" className="nav-link btn btn-primary">Welcome, {props.name} </Link>
          <Link to="/logout" className="nav-link btn btn-primary ml-2">Logout</Link>
        </Fragment>
      )

    authLinks2 = (
        <Fragment>
          <Link to="/members" className="nav-link">Members</Link>
          <Link to="/teams" className="nav-link">Teams</Link>
          <Link to="/tasks" className="nav-link">Tasks</Link>
        </Fragment>
      )
  }

  return (
    <div className="mb-5">
      <Navbar color="dark" light expand="md">
        <NavbarBrand className="font-weight-bold text-white" href="/">MERN Tracker</NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
          { authLinks2 }
          </Nav>
          { authLinks }
        </Collapse>
      </Navbar>
    </div>
  );
}

export default AppNavbar;