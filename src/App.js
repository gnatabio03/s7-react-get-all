import React, { useState } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Redirect } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import "./style.css";

//pages
import AppNavbar from './partials/AppNavbar';
import MembersPage from "./pages/MembersPage";
import LandingPage from "./pages/LandingPage";
import TeamsPage from "./pages/TeamsPage";
import TasksPage from "./pages/TasksPage";
import LoginPage from "./pages/LoginPage";
import RegisterPage from "./pages/RegisterPage";
import ProfilePage from "./pages/ProfilePage";
import NotFoundPage from "./pages/NotFoundPage";

const App = () => {

	const [appData, setAppData] = useState({
		token: localStorage.token,
		name: localStorage.name
	})

	const { token, name } = appData;

	const getCurrentMember = () => {
		return { name, token }
	}

	const Load = (props, page) => {

		//Landing Page
		if(page === "LandingPage") {
			return <LandingPage {...props} />
		}

		//LoginPage or RegisterPage

		// if((page === "LoginPage" || page === "RegisterPage") && token ) return <Redirect to="/profile" />

		if(page === "LoginPage" && token) {
			return <Redirect to="/profile" />
		} else if (page === "LoginPage") {
			return <LoginPage {...props} />
		}

		if(page === "RegisterPage" && token) {
			return <Redirect to="/profile" />
		} else if (page === "RegisterPage") {
			return <RegisterPage {...props} />
		}

		if(!token)	return <Redirect to="/login" />

		if(page === "LogoutPage") {
			localStorage.clear()
			setAppData({
				token,
				name
			})
			return window.location="/login"
		}


		switch(page) {
			case "MembersPage": return <MembersPage {...props} getCurrentMember={ getCurrentMember() }/>
			case "TeamsPage": return <TeamsPage {...props} token={ token } />
			case "TasksPage": return <TasksPage {...props} getCurrentMember={ getCurrentMember()} />
			case "ProfilePage": return <ProfilePage {...props} getCurrentMember={ getCurrentMember()} />
			default: return <NotFoundPage/>
		}
	}


	return (
	<BrowserRouter>
		<AppNavbar token={ token } name={ name } getCurrentMember={ getCurrentMember() }/>
		<Switch>
			<Route path="/members" render={ (props)=> Load(props, "MembersPage")} />
			<Route path="/teams" render={ (props)=> Load(props, "TeamsPage")} />
			<Route path="/login" render={ (props)=> Load(props, "LoginPage") }/>
			<Route path="/profile" render={ (props)=> Load(props, "ProfilePage")} />
			<Route path="/tasks" render={ (props)=> Load(props, "TasksPage")} />
			<Route path="/register" render={ (props)=> Load(props, "RegisterPage") }/>
			<Route path="/logout" render={ (props)=> Load(props, "LogoutPage") } />
			<Route exact path="/" render={ (props)=> Load(props, "LandingPage")} />
			<Route path="*" render={ (props)=> Load(props, "NotFoundPage") } />
		</Switch>
	</BrowserRouter>
	)
}

export default App;
