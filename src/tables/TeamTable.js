import React from 'react';
import { Table } from 'reactstrap';
import TeamRow from "../rows/TeamRow";

const TeamTable = (props) => {
  console.log(props.teams)
  let row;

  if(!props.teams) {
    row = (
      <tr colSpan="3">
        <em>No teams found...</em>
      </tr>
    )
  } else {
    let i = 0;
    row = (
        props.teams.map(team => {
          return <TeamRow teams={team} key={team._id} index={++i} />  
        })
      )
  }
  return (
    <Table responsive hover borderless size="sm" className="p-4 rounded">
      <thead>
        <tr>
          <th>#</th>
          <th>Name</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        { row }
      </tbody>
    </Table>
  );
}

export default TeamTable;