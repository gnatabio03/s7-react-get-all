import React from 'react';
import { Table } from 'reactstrap';
import TaskRow from "../rows/TaskRow";

const TaskTable = (props) => {
  return (
    <Table responsive hover borderless size="sm" className="p-4 rounded">
      <thead>
        <tr>
          <th>#</th>
          <th>Description</th>
          <th>Member Id</th>
        </tr>
      </thead>
      <tbody>
        <TaskRow/>
      </tbody>
    </Table>
  );
}

export default TaskTable;