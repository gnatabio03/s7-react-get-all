import React from 'react';
import { Table } from 'reactstrap';
import MemberRow from "../rows/MemberRow";

const MemberTable = (props) => {
  return (
    <Table responsive hover borderless size="sm" className="p-4 rounded">
      <thead>
        <tr>
          <th>#</th>
          <th>Username</th>
          <th>Email</th>
          <th>Team</th>
          <th>Position</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        <MemberRow/>
      </tbody>
    </Table>
  );
}

export default MemberTable;