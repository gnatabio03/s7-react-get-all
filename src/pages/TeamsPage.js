import React, { useState, useEffect } from 'react';
import { Container, Row, Col } from 'reactstrap';
import TeamForm from "../forms/TeamForm";
import TeamTable from "../tables/TeamTable";
import axios from 'axios';

const TeamsPage = (props) => {
	console.log(props.token)

	const [ teamsData, setTeamsData] = useState({
		token: props.token,
		teams: []
	})

	const { token, teams } = teamsData;
	console.log(teams)

	const getTeams = async () => {
		try {
			const config = {
				headers: {
					Authorization: `Bearer ${token}`
				}
			}

			const res = await axios.get("http://localhost:4000/teams", config)
			console.log(res)
			setTeamsData({
				teams: res.data
			})
		} catch(e) {
			console.log(e.response)
			//SWAL
		}
	}
	// getTeams()
	useEffect(()=> {
		getTeams()
	}, [setTeamsData])

	return (
		<Container>
			<Row className="my-5">
				<Col className="mb-3">
					<h1>TeamsPage</h1>
				</Col>
			</Row>
			<Row>
				<Col md="4" className="border">
					<TeamForm/>
				</Col>
				<Col className="border">
					<TeamTable teams={ teams }/>
				</Col>
			</Row>
		</Container>
	);
}

export default TeamsPage;