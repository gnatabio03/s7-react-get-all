import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import MemberForm from "../forms/MemberForm";
import MemberTable from "../tables/MemberTable";
// import MemberRow from "../rows/MemberRow";

const MembersPage = (props) => {
  console.log(props.getCurrentMember)
  return (
    <Container>
      <Row className="mb-4">
        <Col>
          <h1>Members { props.name }</h1>
        </Col>
      </Row>
      <Row>
        <Col md="4" className="border">
            <MemberForm/>
        </Col>
        <Col className="border">
            <MemberTable/>
        </Col>
      </Row>
    </Container>
  );
}

export default MembersPage;