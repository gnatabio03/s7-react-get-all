import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import TaskForm from "../forms/TaskForm";
import TaskTable from "../tables/TaskTable";

const TasksPage = (props) => {
	console.log(props.getCurrentMember)
	return (
		<Container>
			<Row className="my-5">
				<Col className="mb-3">
					<h1>TasksPage</h1>
				</Col>
			</Row>
			<Row>
				<Col md="4" className="border">
					<TaskForm/>
				</Col>
				<Col className="border">
					<TaskTable/>
				</Col>
			</Row>
		</Container>
	);
}

export default TasksPage;