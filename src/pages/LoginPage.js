import React from 'react';
import { Redirect } from 'react-router-dom';
import { Container, Row, Col } from 'reactstrap';
import LoginForm from "../forms/LoginForm";

const LoginPage = (props) => {
	console.log("LoginPage props", props.token)
	// if(props.token) {
	// 	return <Redirect to="/profile" />
	// }
	return (
		<Container>
			<Row className="my-5">
				<Col className="mb-3">
					<h1>Login Page</h1>
				</Col>
			</Row>
			<Row>
				<Col className="border">
					<LoginForm/>
				</Col>
			</Row>
		</Container>
	);
}

export default LoginPage;