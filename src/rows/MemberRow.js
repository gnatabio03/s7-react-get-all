import React, { Fragment } from 'react';
import { Button } from 'reactstrap';

const MemberRow = (props) => {
  return (
    <Fragment>
        <tr>
          <th scope="row">1</th>
          <td>gnatabio</td>
          <td>gnatabio@gmail.com</td>
          <td>Team 1</td>
          <td>Admin</td>
          <td>
              <button className="btn btn-primary"><i className="far fa-eye"></i></button>
              <button className="btn btn-warning"><i className="fas fa-edit"></i></button>
              <button className="btn btn-danger"><i className="fas fa-trash-alt"></i></button>
          </td>
        </tr>
        <tr>
          <th scope="row">2</th>
          <td>gelo</td>
          <td>gelo@gmail.com</td>
          <td>Team 2</td>
          <td>Student</td>
          <td>
              <button className="btn btn-primary"><i className="far fa-eye"></i></button>
              <button className="btn btn-warning"><i className="fas fa-edit"></i></button>
              <button className="btn btn-danger"><i className="fas fa-trash-alt"></i></button>
          </td>
        </tr>
        <tr>
          <th scope="row">3</th>
          <td>geodz</td>
          <td>geodel@gmail.com</td>
          <td>Team 3</td>
          <td>Student</td>
          <td>
              <button className="btn btn-primary"><i className="far fa-eye"></i></button>
              <button className="btn btn-warning"><i className="fas fa-edit"></i></button>
              <button className="btn btn-danger"><i className="fas fa-trash-alt"></i></button>
          </td>
        </tr>
    </Fragment> 
  );
}

export default MemberRow;