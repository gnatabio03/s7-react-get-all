import React from 'react';
import { Button } from 'reactstrap';

const TeamRow = (props) => {
  console.log(props.teams)

  const team = props.teams
  return (
        <tr>
          <th scope="row">{ props.index }</th>
          <td>{ team.name }</td>
          <td>
              <Button className="btn" color="info"><i className="far fa-eye"></i></Button>
              <Button className="btn" color="warning"><i className="fas fa-edit"></i></Button>
              <Button className="btn" color="danger"><i className="fas fa-trash-alt"></i></Button>
          </td>
        </tr>
  );
}

export default TeamRow;