import React, { Fragment } from 'react';
import { Button } from 'reactstrap';

const TaskRow = (props) => {
  return (
    <Fragment>
        <tr>
          <th scope="row">1</th>
          <td>Go home</td>
          <td>asdasdasdasd</td>
          <td>
              <button className="btn btn-primary"><i className="far fa-eye"></i></button>
              <button className="btn btn-warning"><i className="fas fa-edit"></i></button>
              <button className="btn btn-danger"><i className="fas fa-trash-alt"></i></button>
          </td>
        </tr>
        <tr>
          <th scope="row">2</th>
          <td>Go to work</td>
          <td>asdasdasdasd</td>
          <td>
              <button className="btn btn-primary"><i className="far fa-eye"></i></button>
              <button className="btn btn-warning"><i className="fas fa-edit"></i></button>
              <button className="btn btn-danger"><i className="fas fa-trash-alt"></i></button>
          </td>
        </tr>
    </Fragment> 
  );
}

export default TaskRow;