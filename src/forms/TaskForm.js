import React from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';

const TaskForm = (props) => {
	return (
		<Form>
			<FormGroup>
				<Label for="description">Description</Label>
	        	<Input type="textarea" name="description" id="description"/>
			</FormGroup>
			<FormGroup>
				<Label for="memberId">memberId</Label>
				<Input type="select" name="memberId" id="memberId">
				  <option>1</option>
				  <option>2</option>
				  <option>3</option>
				  <option>4</option>
				  <option>5</option>
				</Input>
			</FormGroup>
			<Button className="btn btn-block" color="primary">Create Task</Button>
		</Form>
	);
}

export default TaskForm;