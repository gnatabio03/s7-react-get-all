import React, { useState, useEffect } from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { Link, Redirect } from 'react-router-dom';
import axios from 'axios';
import Swal from 'sweetalert2';

const RegisterForm = (props) => {
  
  //STATE
  //object that holds a component's dynamic data
  // console.log(useState("initial value"))
  // const [username, setUsername] = useState("test");
  // console.log(username)
  // //username is state, the value is test, and setUsername is the method that updates the state or will handle the changes
  // const [email, setEmail] = useState("test1");
  // console.log(email)

  const [formData, setFormData] = useState({
    username: "",
    email: "",
    password: "",
    password2: ""
  });

  const [disabled, setDisabled] = useState(true)

  const [isRedirected, setIsRedirected] = useState(false)

  const { username, email, password, password2 } = formData;

  const onChangeHandler = e => {
    // console.log(e);
    // console.log(e.target);

    //Backtick - multi-line and string interpolation
    // console.log(`e.target.name : ${e.target.name}`)
    // console.log(`e.target.value: ${e.target.value}`)

    //update the state
    setFormData({
      ...formData,
      [e.target.name] : e.target.value
    })
    console.log(formData)
  }

  const onSubmitHandler = async e => {
    e.preventDefault();
    //check if passwords match
    if(password !== password2) {
      console.log("passwords don't match!")
      Swal.fire({
        title: "Error!",
        text: "Passwords don't match",
        icon: "error",
        showConfirmButton: false,
        timer: 2000
      })
    } else {
      //submit to backend
      console.log(formData)

      //create a member object
      const newMember = {
        username,
        email,
        password
      }
      
      try {
        //create a config
        const config = {
          headers: {
            'Content-Type': 'application/json'
          }
        }

        //create the body
        const body = JSON.stringify(newMember)

        //access route using axios
        const res = await axios.post("http://localhost:4000/members", body, config)

        console.log(res)

        //sweetalert success
        Swal.fire({
          title: "Success!",
          text: "Registered Successfully",
          icon: "success",
          showConfirmButton: false,
          timer: 2000
        })

        //redirect to login
        // <Redirect to="/login"/>
        setIsRedirected(true)
      } catch(e) {
        //sweetalert - error
        console.log(e.message)
        Swal.fire({
          title: "Error!",
          text: "Member already exist!",
          icon: "error",
          showConfirmButton: false,
          timer: 2000
        })
      } 
      
    }
  }

  //USE EFFECT
  //hook that tells the component what to do after render
  useEffect(()=> {
    if(username !== "" && email !== "" && password !== "" && password2 !== ""){
      setDisabled(false)
    } else {
      setDisabled(true)
    }
  }, [formData])

  if(isRedirected){
    return <Redirect to="/login" />
  }

  return (
    <Form onSubmit = { e => onSubmitHandler(e)}>
      <FormGroup>
        <Label for="username">Username</Label>
        <Input type="text" 
        name="username" 
        id="username" 
        value={username} 
        onChange={ e => onChangeHandler(e)} 
        maxLength="30" 
        pattern="[a-zA-Z0-9]+" 
        required
        />
        <FormText>Use alphanumeric characters only.</FormText>
      </FormGroup>
      <FormGroup>
        <Label for="email">Email</Label>
        <Input type="email" 
        name="email" 
        id="email" 
        value={email} 
        onChange={ e => onChangeHandler(e)}
        required
        />
      </FormGroup>
      <FormGroup>
        <Label for="password">Password</Label>
        <Input type="password" 
        name="password" 
        id="password" 
        value={password} 
        onChange={ e => onChangeHandler(e)}
        required
        minLength="5"
        />
      </FormGroup>
      <FormGroup>
        <Label for="password2">Confirm Password</Label>
        <Input type="password" 
        name="password2" 
        id="password2" 
        value={password2} 
        onChange={ e => onChangeHandler(e)}
        required
        minLength="5"
        />
      </FormGroup>
      <Button 
        className="btn btn-block mb-3" 
        color="primary" 
        disabled={disabled}
        >Submit
      </Button>
      <p>
      	Already have an account? <Link to="/login">Go to Login</Link>
      </p>
    </Form>
  );
}

export default RegisterForm;