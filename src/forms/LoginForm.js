import React, { useState, useEffect } from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { Redirect } from 'react-router-dom';
import axios from 'axios';
import Swal from 'sweetalert2';

const LoginForm = (props) => {

	const [formData, setFormData] = useState({
		email: "",
		password: ""
	}) 

	const [disabled, setDisabled] = useState(true)

	const [isRedirected, setIsRedirected] = useState(false)

	const {email, password} = formData;

	const onChangeHandler = e => {

		setFormData({
			...formData, 
			[e.target.name] : e.target.value
		})
		console.log(formData)
	}

	const onSubmitHandler = async e => {
	  e.preventDefault();
	  console.log(formData)

	  try {
	  	const config = {
	  		headers: {
	  			'Content-type' : 'application/json'
	  		}
	  	}

	  	const body = JSON.stringify(formData)
	  	console.log(body)
	  	const res = await axios.post("http://localhost:4000/members/login", body, config)

	  	console.log(res.data.member.firstName)
        localStorage.setItem("token", res.data.token)
        localStorage.setItem("name", res.data.member.username)

		Swal.fire({
          title: "Success!",
          text: "Login Success!",
          icon: "success",
          showConfirmButton: false,
          timer: 2000
        })
		console.log(res.data)

		setIsRedirected(true)	  	
	  } catch(e) {
	  	localStorage.removeItem("token")
	  	localStorage.removeItem("name")
	  	console.log(e)
	  	Swal.fire({
          title: "Error!",
          text: "Invalid Login Credentials!",
          icon: "error",
          showConfirmButton: false,
          timer: 2000
        })
	  }
	}

	useEffect(()=> {
	    if(email !== "" && password !== ""){
	      setDisabled(false)
	    } else {
	   	setDisabled(true)
	   	}
	}, [formData])

	if(isRedirected){
	  // return <Redirect to="/profile" />
	  window.location = "/profile"
	}

	return (
		<Form className="mb-4" onSubmit = { e => onSubmitHandler(e)}>
			<FormGroup>
		        <Label for="email">Email</Label>
		        <Input type="email" 
		        name="email" 
		        id="email"
		        value={email}
		        onChange={ e => onChangeHandler(e)}
		        required
		        />
			</FormGroup>
			<FormGroup>
		        <Label for="password">Password</Label>
		        <Input type="password" 
		        name="password" 
		        id="password"
		        value={password}
		        onChange={ e => onChangeHandler(e)}
		        required
		        />
		    </FormGroup>
			<Button 
			className="btn btn-block" 
			color="primary"
			disabled={disabled}
			>Login
			</Button>
		</Form>
	);
}

export default LoginForm;