import React from 'react';
import { Button, Form, FormGroup, Label, Input, } from 'reactstrap';

const Example = (props) => {
  return (
    <Form>
      <FormGroup>
        <Label for="username">Username</Label>
        <Input type="text" name="username" id="username" placeholder="" disabled/>
      </FormGroup>
      <FormGroup>
        <Label for="email">Email</Label>
        <Input type="email" name="email" id="email" placeholder="" disabled/>
      </FormGroup>
      <FormGroup>
        <Label for="team">Team</Label>
        <Input type="select" name="team" id="team">
          <option>1</option>
          <option>2</option>
          <option>3</option>
          <option>4</option>
          <option>5</option>
        </Input>
      </FormGroup>
      <FormGroup>
        <Label for="position">Position</Label>
        <Input type="select" name="position" id="position">
          <option>1</option>
          <option>2</option>
          <option>3</option>
          <option>4</option>
          <option>5</option>
        </Input>
      </FormGroup>
   
      <Button className="btn btn-block" color="primary">Save Changes</Button>
    </Form>
  );
}

export default Example;