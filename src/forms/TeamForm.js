import React, { useState, useEffect } from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import axios from 'axios';
import Swal from 'sweetalert2';

const TeamForm = (props) => {

	const [formData, setFormData] = useState({
		name: ""
	})

	const [disabled, setDisabled] = useState(true)

	const { name } = formData;

	const onChangeHandler = e => {
		setFormData({
			[e.target.name] : e.target.value
		})
		console.log(formData)
	}

	const onSubmitHandler = async e => {
		e.preventDefault();
	  	console.log(formData)

	  	//create a new team
	  	const newTeam = { name }

	  	try {
		  	const config = {
		  		headers: {
		  			'Content-Type': 'application/json'
		  		}	
		  	}
		  	const body = JSON.stringify(newTeam)

		  	const res = await axios.post("http://localhost:4000/teams", body, config)
		  	console.log(res) 
		  	Swal.fire({
		  	  title: "Success!",
		  	  text: "Team Created!",
		  	  icon: "success",
		  	  showConfirmButton: false,
		  	  timer: 2000
		  	})
	  	} catch(e) {
	  		console.log(e)
	  	}
	}

	useEffect(()=> {
	    if(name !== ""){
	      setDisabled(false)
	    } else {
	   	setDisabled(true)
	   	}
	}, [formData])

	return (
		<Form onSubmit = { e => onSubmitHandler(e)}>
			<FormGroup>
				<Label for="name">Name</Label>
	        	<Input type="text" 
	        	name="name" 
	        	id="name"
	        	value={name}
	        	onChange={ e => onChangeHandler(e)}
	        	required/>
			</FormGroup>
			<Button className="btn btn-block" 
			color="primary"
			disabled={disabled}
			>Create Team
			</Button>
		</Form>
	);
}

export default TeamForm;